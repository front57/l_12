function onLoadHandler(evt){
    const innerItems = document.querySelectorAll(evt.currentTarget.itemClassName);
    [...innerItems].forEach(e => {
        const rect = e.getBoundingClientRect();
        const max = window.innerHeight - (Math.abs(evt.currentTarget.tMargin) + Math.abs(evt.currentTarget.bMargin));
        const lineWidth = 100 - rect.top / max * 100;
        e.parentElement.style.background = `linear-gradient(to right, ${evt.currentTarget.lineColor} ${lineWidth}%, 
        #fff 0)`;
    });
}

function drawLine(lineColor){
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                //entry.target.style.height = Math.abs(tMargin) * 2 + 'px';
                window.addEventListener('scroll', function(){
                    const rect = entry.target.getBoundingClientRect();
                    const max = window.innerHeight - (Math.abs(tMargin) + Math.abs(bMargin));
                    const lineWidth = 100 - rect.top / max * 100;
                    entry.target.parentElement.style.background = `linear-gradient(to right, ${lineColor} ${lineWidth}%, 
                    #fff 0)`;
                });

            }
        })
    }

    const tMargin = -(window.innerHeight / 100 * 30);
    const bMargin = -(window.innerHeight / 100 * 20);
    const itemClassName = '.item-delimiter__inner';
    const opts = {
        rootMargin: `${tMargin}px 0px ${bMargin}px`,
        threshold: [0.5],
    };
    const observer = new IntersectionObserver(cb, opts);
    const target = document.querySelectorAll(itemClassName);
    target.forEach(e => {
        e.style.height = Math.abs(tMargin) * 2 + 'px';
        observer.observe(e);
    })
    window.addEventListener('DOMContentLoaded', onLoadHandler, false);
    window.tMargin = tMargin;
    window.bMargin = bMargin;
    window.itemClassName = itemClassName;
    window.lineColor = lineColor;
}

drawLine('#000');

function showTagline(){
    let sleep = 0;
    const displayTagline = (entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting){
                setTimeout(() => {
                    entry.target.classList.add('horizontal-normalization');
                }, sleep += 250);
            }
        })
    }
    const opts = {
        threshold: 1,
    }
    const observer = new IntersectionObserver(displayTagline, opts);
    const elems = document.querySelectorAll('.services__p-slogan');
    elems.forEach(e => {
        observer.observe(e);
    })
}
showTagline();

function scaleCircles(){

    const startCirclesAnimation = (entries, observer) => {
        entries.forEach( entry => {
            if(entry.isIntersecting){
                if (entry.target.classList.contains('services__circle--middle') && window.innerWidth > 768){
                    entry.target.style.animation = 'scaleMiddleCircles 1s ease-in-out'
                    return;
                }
                entry.target.style.animation = 'scaleCircles 1s ease-in-out';
            }

        });
    };

    const opts = {
        threshold:1,
    }
    const observer = new IntersectionObserver(startCirclesAnimation, opts);
    const elems = document.querySelectorAll('.services__circle');
    elems.forEach(e => {
        observer.observe(e);
    })
}

scaleCircles();