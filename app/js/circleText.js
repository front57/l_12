function startCircleTxtAnimation(){
    const text = document.querySelector('.hero__top-circle-paragraph');
    text.style.opacity = '1';
    text.innerHTML = text.innerText.split("").map(
        (char, i) =>
            `<span style="transform:rotate(${i * 8.3}deg)">${char}</span>`
    ).join('');
}

addEventListener("load", (event) => {
    startCircleTxtAnimation();
});