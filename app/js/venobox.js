const venoBox = new VenoBox({
    selector: '.reviews__gratitude-img-wrapper',
    numeration: true,
    infinigall: true,
    share: true,
    spinner: 'rotating-plane'
});