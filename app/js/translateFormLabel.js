function translateFormLabel(){
    const inputs = document.getElementsByClassName('feedback__input');

    [...inputs].forEach(e => e.addEventListener('focus', function(){
        this.nextElementSibling.classList.add('active');
    }));

    [...inputs].forEach(e => e.addEventListener('focusout', function(){
        if(this.value === ''){
            this.nextElementSibling.classList.remove('active');
        }
    }));
}

translateFormLabel();