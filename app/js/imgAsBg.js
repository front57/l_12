function imagesToBg(){
    const images = document.querySelectorAll('.background-img');

    images.forEach(e => {
        const imgSrc = e.src;
        e.parentElement.style.backgroundImage = `url('${imgSrc}')`;
        e.parentElement.classList.add('background-center');
        e.style.display = 'none';

    });
}

imagesToBg();