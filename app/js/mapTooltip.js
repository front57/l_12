const paths = document.querySelectorAll('[data-city]');

window.addEventListener('scroll', function(){
    const tooltips = document.querySelectorAll('.portfolio__tooltip');
    tooltips.forEach(e => e.remove());
});

paths.forEach(e => e.addEventListener('mouseover', function(){
    const rect = this.getBoundingClientRect();
    const tooltip = createTooltip( this.dataset.city);
    const verticalOffset = window.innerWidth > 576 ? 75 : 50;

    document.body.appendChild(tooltip);
    tooltip.style.top = rect.bottom - verticalOffset + 'px';
    tooltip.style.left = rect.right + 'px';
    tooltip.style.zIndex = '1';
    this.addEventListener('mouseout', removeTooltiop);
}));

function createTooltip(cityName){
    const tooltip = document.createElement("div");
    tooltip.className = 'portfolio__tooltip';
    tooltip.innerHTML = `
    <p></p>
    <div class="portfolio__tooltip-arrow">
        <div class="portfolio__tooltip-corner--topright"></div>
        <div class="portfolio__tooltip-corner--topleft"></div>
    </div>
  `;
    tooltip.querySelector('p').innerText = cityName;
    return tooltip;
}

function removeTooltiop(){
    const tooltips = document.querySelectorAll('.portfolio__tooltip');
    tooltips.forEach(e => {
        fadeElement(e)
    });
}
/*
function showElement(elem, opacity = 0){
  if(opacity < 1){
    opacity += .1;
    setTimeout(function(){
      showElement(elem, opacity);
    }, 50);
  }
  elem.style.opacity = opacity;
}*/
function fadeElement(elem, opacity = 1){
    if (opacity > -.1){
        opacity -= .1;
        setTimeout(function(){
            fadeElement(elem, opacity);
        }, 75);
    } else {
        elem.remove();
    }
    elem.style.opacity = opacity;

}