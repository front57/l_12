
const qualityTopSwiper = new Swiper(".quality__slider--top", {
    slidesPerView:1,
    slidesPerScroll:1,
    spaceBetween: 24,
    autoHeight: true,
    effect: "fade",
    fadeEffect: {
        crossFade: true
    },
    pagination: {
        el: ".slider-nav__pagination",
        type: "fraction",
        formatFractionCurrent: function (number) {
            return ('0' + number);
        },
        formatFractionTotal: function (number) {
            return ('0' + number);
        },
        renderFraction: function (currentClass, totalClass) {
            return '<span class="' + currentClass + '"></span>' +
                ' / ' +
                '<span class="' + totalClass + '"></span>';
        }
    },
    navigation: {
        nextEl: ".slider-nav__btn--next",
        prevEl: ".slider-nav__btn--prev",
    },
});

const qualityBottomSwiper = new Swiper(".quality__slider--bottom", {
    slidesPerView:1,
    slidesPerScroll:1,
    spaceBetween: 24,
    autoHeight: true,
    effect: "fade",
    fadeEffect: {
        crossFade: true
    },
});

qualityTopSwiper.controller.control = qualityBottomSwiper;
qualityBottomSwiper.controller.control = qualityTopSwiper;

const portfolioSwiper1 = new Swiper(".portfolio__slider--first", {
        slidesPerView:1,
        slidesPerScroll:1,
        spaceBetween: 24,

        breakpoints: {
            320:{
                spaceBetween: 8,
            },
            576: {
                spaceBetween: 16,
            },
            768: {
                slidesPerView:1.1,
            },
            992: {
                slidesPerView:1,
            },
        },
        pagination: {
            el: ".portfolio__slider-pagination--first",
            type: "fraction",
            formatFractionCurrent: function (number) {
                return ('0' + number);
            },
            formatFractionTotal: function (number) {
                return ('0' + number);
            },
            renderFraction: function (currentClass, totalClass) {
                return '<span class="' + currentClass + '"></span>' +
                    ' / ' +
                    '<span class="' + totalClass + '"></span>';
            }
        },
        navigation: {
            nextEl: ".portfolio__slider-btn-next--first",
            prevEl: ".portfolio__slider-btn-prev--first",
        },


    });
const portfolioSwiper2 = new Swiper(".portfolio__slider--second", {
    slidesPerView:1,
    slidesPerScroll:1,
    //spaceBetween: 24,
    spaceBetween: 24,
    breakpoints: {
        320:{
            spaceBetween: 8,
        },
        576: {
            spaceBetween: 16,
        },
        768: {
            slidesPerView:1.1,
        },
        992: {
            slidesPerView:1,
        },
    },
    pagination: {
        el: ".portfolio__slider-pagination--second",
        type: "fraction",
        formatFractionCurrent: function (number) {
            return ('0' + number);
        },
        formatFractionTotal: function (number) {
            return ('0' + number);
        },
        renderFraction: function (currentClass, totalClass) {
            return '<span class="' + currentClass + '"></span>' +
                ' / ' +
                '<span class="' + totalClass + '"></span>';
        }
    },
    navigation: {
        nextEl: ".portfolio__slider-btn-next--second",
        prevEl: ".portfolio__slider-btn-prev--second",
    },


});
const portfolioSwiper3 = new Swiper(".portfolio__slider--third", {
    slidesPerView:1,
    slidesPerScroll:1,
    //spaceBetween: 24,
    spaceBetween: 24,
    breakpoints: {
        320:{
            spaceBetween: 8,
        },
        576: {
            spaceBetween: 16,
        },
        768: {
            slidesPerView:1.1,
        },
        992: {
            slidesPerView:1,
        },
    },
    pagination: {
        el: ".portfolio__slider-pagination--third",
        type: "fraction",
        formatFractionCurrent: function (number) {
            return ('0' + number);
        },
        formatFractionTotal: function (number) {
            return ('0' + number);
        },
        renderFraction: function (currentClass, totalClass) {
            return '<span class="' + currentClass + '"></span>' +
                ' / ' +
                '<span class="' + totalClass + '"></span>';
        }
    },
    navigation: {
        nextEl: ".portfolio__slider-btn-next--third",
        prevEl: ".portfolio__slider-btn-prev--third",
    },


});
const portfolioSwiper4 = new Swiper(".portfolio__slider--fourth", {
    slidesPerView:1,
    slidesPerScroll:1,
    //spaceBetween: 24,
    spaceBetween: 24,
    breakpoints: {
        320:{
            spaceBetween: 8,
        },
        576: {
            spaceBetween: 16,
        },
        768: {
            slidesPerView:1.1,
        },
        992: {
            slidesPerView:1,
        },
    },
    pagination: {
        el: ".portfolio__slider-pagination--fourth",
        type: "fraction",
        formatFractionCurrent: function (number) {
            return ('0' + number);
        },
        formatFractionTotal: function (number) {
            return ('0' + number);
        },
        renderFraction: function (currentClass, totalClass) {
            return '<span class="' + currentClass + '"></span>' +
                ' / ' +
                '<span class="' + totalClass + '"></span>';
        }
    },
    navigation: {
        nextEl: ".portfolio__slider-btn-next--fourth",
        prevEl: ".portfolio__slider-btn-prev--fourth",
    },


});
const portfolioSwiper5 = new Swiper(".portfolio__slider--fifth", {
    slidesPerView:1,
    slidesPerScroll:1,
    //spaceBetween: 24,
    spaceBetween: 24,
    breakpoints: {
        320:{
            spaceBetween: 8,
        },
        576: {
            spaceBetween: 16,
        },
        768: {
            slidesPerView:1.1,
        },
        992: {
            slidesPerView:1,
        },
    },
    pagination: {
        el: ".portfolio__slider-pagination--fifth",
        type: "fraction",
        formatFractionCurrent: function (number) {
            return ('0' + number);
        },
        formatFractionTotal: function (number) {
            return ('0' + number);
        },
        renderFraction: function (currentClass, totalClass) {
            return '<span class="' + currentClass + '"></span>' +
                ' / ' +
                '<span class="' + totalClass + '"></span>';
        }
    },
    navigation: {
        nextEl: ".portfolio__slider-btn-next--fifth",
        prevEl: ".portfolio__slider-btn-prev--fifth",
    },


});

const reviewsSwiper = new Swiper(".reviews__slider", {
    slidesPerView:1,
    slidesPerScroll:1,
    spaceBetween: 24,
    autoHeight: true,
    pagination: {
        el: ".reviews__slider-pagination",
        type: "fraction",
        formatFractionCurrent: function (number) {
            return ('0' + number);
        },
        formatFractionTotal: function (number) {
            return ('0' + number);
        },
        renderFraction: function (currentClass, totalClass) {
            return '<span class="' + currentClass + '"></span>' +
                ' / ' +
                '<span class="' + totalClass + '"></span>';
        }
    },
    navigation: {
        nextEl: ".reviews__slider-btn-next",
        prevEl: ".reviews__slider-btn-prev",
    },


});

const gratitudeSwiper = new Swiper(".reviews__gratitude-slider", {
    slidesPerView:2,
    slidesPerScroll:2,
    spaceBetween: 32,
    autoHeight: true,
    breakpoints: {
        320:{
            spaceBetween: 8,
            slidesPerView:1.1,
        },
        576: {
            spaceBetween: 16,
            slidesPerView: 1.5,
        },
        992: {
            slidesPerView:2,
        },
    },

});
