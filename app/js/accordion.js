const acc = document.getElementsByClassName("accordion__header");
const panels = document.querySelectorAll('.accordion__body');


/*acc.forEach(e => e.addEventListener('click', function () {
    e.parentElement.classList.toggle('active');
    panels.forEach(e => {

    });
}));*/

for (let i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        panels.forEach((e,idx) => {
            if (idx !== i){
                e.style.maxHeight = null;
                e.previousElementSibling.classList.remove('active');
            }else{
                e.style.maxHeight = e.style.maxHeight ? null : e.scrollHeight + "px";
                e.previousElementSibling.classList.toggle('active');
            }
        });
    });
}