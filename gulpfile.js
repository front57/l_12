const app = 'app/',
    dist = 'dist/';


const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const del = require('del');
const concat = require('gulp-concat');
const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const cleanCss = require('gulp-clean-css');
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const include = require('gulp-file-include');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const size = require('gulp-size');
const sourcemap = require('gulp-sourcemaps');
const uglify = require('gulp-uglify-es').default;

const config = {
    app:{
        //**/!(_|parts)*.html
        html: app + '!(_|parts)*.html',
        style: app + 'scss/main.scss',
        js: app + 'js/scripts.js',
        fonts: app + 'fonts/**/*.*',
        img: app + 'img/**/*.*',
    },
    dist:{
        html: dist,
        style: dist + 'css/',
        js: dist + 'js/',
        fonts: dist + 'fonts/',
        img: dist + 'img/',
    },
    watch:{
        html: app + '*.html',
        style: app + 'scss/**/*.scss',
        js: app + 'js/**/*.*',
        fonts: app + 'fonts/**/*.*',
        img: app + 'img/**/*.*',
    }
};


const html = () => {
    return gulp.src(config.app.html)
        .pipe(include({ prefix: '@@', path: '@file'}))
        .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
        .pipe(size())
        .pipe(gulp.dest(config.dist.html))
        .pipe(browserSync.stream())
};

const styles = () => {
    return gulp.src(config.app.style)
        .pipe(sourcemap.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({overrideBrowserslist: ['last 5 versions'], cascade: false}))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCss())
        .pipe(sourcemap.write("."))
        .pipe(size())
        .pipe(gulp.dest(config.dist.style))
        .pipe(browserSync.stream())
}

const scripts = () => {
    return gulp.src(config.app.js)
        .pipe(include({ prefix: '@@', path: '@file'}))
        .pipe(sourcemap.init())
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemap.write("."))
        .pipe(size())
        .pipe(gulp.dest(config.dist.js))
        .pipe(browserSync.stream())
}

const img = () => {
    return gulp.src(config.app.img)
        .pipe(newer(config.dist.img))
        .pipe(imagemin({
            progressive:true,
        }))
        .pipe(size())
        .pipe(gulp.dest(config.dist.img))
        .pipe(browserSync.stream())
}

const fonts = () => {
    return gulp.src(config.app.fonts)
        .pipe(gulp.dest(config.dist.fonts))
        .pipe(browserSync.stream())
}

const clear = () => {
    return del([dist+'**/*', '!' + dist + 'img']);
}

const clearAll = () => {
    return del(dist);
}

const watch = () => {
    browserSync.init({
        server: {
            baseDir: dist
        }
    });
    gulp.watch(config.watch.style, styles);
    gulp.watch(config.watch.html).on('change', browserSync.reload);
    gulp.watch(config.watch.js, scripts);
    gulp.watch(config.watch.html, html);
    gulp.watch(config.watch.img, img);
    gulp.watch(config.watch.fonts, fonts);
}

const build = gulp.series(clear, gulp.parallel(html, styles, scripts, img, fonts));

exports.html = html;
exports.clear = clear;
exports.clear_a = clearAll;
exports.default = gulp.series(build, watch);